import React, { Component } from 'react';
//import Moment from 'moment';
import { Button, tr, tbody, table } from 'react-bootstrap';
import Select from 'react-select';
import axios from 'axios';
import Moment from 'moment';

const styles = {

    tableColTitle: {
        fontSize: 25,
        color: '#FFFFFF',
        textAlign: 'center',
        border: 'none',

    },
    tableCell: {
        fontSize: 20,
        color: '#6C6B69',
        textAlign: 'center',
        margin: 'auto',
        verticalAlign: 'middle'
    },

    tableCellWithAlert: {
        fontSize: 20,
        color: '#FFFFFF',
        textAlign: 'center',
        margin: 'auto',
        verticalAlign: 'middle'
    },

    tableRowWithAlert: {
        backgroundColor: '#DF013A'
    },
    tableRow: {
        backgroundColor: '#FFFFFF'
    },

    resetButton: {
        backgroundColor: '#6F6B6A',
        color: '#FFFFFF',
        fontSize: 20,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 2,
        paddingBottom: 2,
    },
    container: {
        flex: 1,
        backgroundColor: '#D8CBBB',
        height: window.innerHeight,
    },
    pointIn: {
        borderRadius: 12.5,
        width: 25,
        height: 25,
        backgroundColor: '#18ACA4',
        margin: 'auto'
    },
    pointOff: {
        borderRadius: 12.5,
        width: 25,
        height: 25,
        backgroundColor: '#F2BF55',
        margin: 'auto'
    }
};

const patients = [
    // for stresstest.
    // second floor
    { "residentId": "id-201-1", "bedNo": "201-1",  "residentName": "Name-201-1"},
    { "residentId": "id-201-2", "bedNo": "201-2",  "residentName": "Name-201-2"},
    { "residentId": "id-202-1", "bedNo": "202-1",  "residentName": "Name-202-1"},
    { "residentId": "id-202-2", "bedNo": "202-2",  "residentName": "Name-202-2"},
    { "residentId": "id-202-3", "bedNo": "202-3",  "residentName": "Name-202-3"},
    { "residentId": "id-203-1", "bedNo": "203-1",  "residentName": "Name-203-1"},
    { "residentId": "id-203-2", "bedNo": "203-2",  "residentName": "Name-203-2"},
    { "residentId": "id-203-3", "bedNo": "203-3",  "residentName": "Name-203-3"},
    { "residentId": "id-204-1", "bedNo": "204-1",  "residentName": "Name-204-1"},
    { "residentId": "id-204-2", "bedNo": "204-2",  "residentName": "Name-204-2"},
    { "residentId": "id-204-3", "bedNo": "204-3",  "residentName": "Name-204-3"},
    { "residentId": "id-205-1", "bedNo": "205-1",  "residentName": "Name-205-1"},
    { "residentId": "id-205-2", "bedNo": "205-2",  "residentName": "Name-205-2"},
    { "residentId": "id-206-1", "bedNo": "206-1",  "residentName": "Name-206-1"},
    { "residentId": "id-206-2", "bedNo": "206-2",  "residentName": "Name-206-2"},
    { "residentId": "id-206-3", "bedNo": "206-3",  "residentName": "Name-206-3"},
    { "residentId": "id-207-1", "bedNo": "207-1",  "residentName": "Name-207-1"},
    { "residentId": "id-207-2", "bedNo": "207-2",  "residentName": "Name-207-2"},
    { "residentId": "id-207-3", "bedNo": "207-3",  "residentName": "Name-207-3"},
    { "residentId": "id-208-1", "bedNo": "208-1",  "residentName": "Name-208-1"},
    { "residentId": "id-208-2", "bedNo": "208-2",  "residentName": "Name-208-2"},
    { "residentId": "id-208-3", "bedNo": "208-3",  "residentName": "Name-208-3"},
    { "residentId": "id-208-4", "bedNo": "208-4",  "residentName": "Name-208-4"},
    { "residentId": "id-209-1", "bedNo": "209-1",  "residentName": "Name-209-1"},
    { "residentId": "id-209-2", "bedNo": "209-2",  "residentName": "Name-209-2"},
    { "residentId": "id-209-3", "bedNo": "209-3",  "residentName": "Name-209-3"},
    { "residentId": "id-210-1", "bedNo": "210-1",  "residentName": "Name-210-1"},
    //third floor
    { "residentId": "id-301-1", "bedNo": "301-1",  "residentName": "Name-301-1"},
    { "residentId": "id-301-2", "bedNo": "301-2",  "residentName": "Name-301-2"},
    { "residentId": "id-302-1", "bedNo": "302-1",  "residentName": "Name-302-1"},
    { "residentId": "id-302-2", "bedNo": "302-2",  "residentName": "Name-302-2"},
    { "residentId": "id-302-3", "bedNo": "302-3",  "residentName": "Name-302-3"},
    { "residentId": "id-303-1", "bedNo": "303-1",  "residentName": "Name-303-1"},
    { "residentId": "id-303-2", "bedNo": "303-2",  "residentName": "Name-303-2"},
    { "residentId": "id-303-3", "bedNo": "303-3",  "residentName": "Name-303-3"},
    { "residentId": "id-304-1", "bedNo": "304-1",  "residentName": "Name-304-1"},
    { "residentId": "id-304-2", "bedNo": "304-2",  "residentName": "Name-304-2"},
    { "residentId": "id-304-3", "bedNo": "304-3",  "residentName": "Name-304-3"},
    { "residentId": "id-305-1", "bedNo": "305-1",  "residentName": "Name-305-1"},
    { "residentId": "id-305-2", "bedNo": "305-2",  "residentName": "Name-305-2"},
    { "residentId": "id-306-1", "bedNo": "306-1",  "residentName": "Name-306-1"},
    { "residentId": "id-306-2", "bedNo": "306-2",  "residentName": "Name-306-2"},
    { "residentId": "id-306-3", "bedNo": "306-3",  "residentName": "Name-306-3"},
    { "residentId": "id-307-1", "bedNo": "307-1",  "residentName": "Name-307-1"},
    { "residentId": "id-307-2", "bedNo": "307-2",  "residentName": "Name-307-2"},
    { "residentId": "id-307-3", "bedNo": "307-3",  "residentName": "Name-307-3"},
    { "residentId": "id-308-1", "bedNo": "308-1",  "residentName": "Name-308-1"},
    { "residentId": "id-308-2", "bedNo": "308-2",  "residentName": "Name-308-2"},
    { "residentId": "id-308-3", "bedNo": "308-3",  "residentName": "Name-308-3"},
    { "residentId": "id-308-4", "bedNo": "308-4",  "residentName": "Name-308-4"},
    { "residentId": "id-309-1", "bedNo": "309-1",  "residentName": "Name-309-1"},
    { "residentId": "id-309-2", "bedNo": "309-2",  "residentName": "Name-309-2"},
    { "residentId": "id-309-3", "bedNo": "309-3",  "residentName": "Name-309-3"},
    { "residentId": "id-310-1", "bedNo": "310-1",  "residentName": "Name-310-1"},
    //fifth floor
    { "residentId": "id-501-1", "bedNo": "501-1",  "residentName": "Name-501-1"},
    { "residentId": "id-501-2", "bedNo": "501-2",  "residentName": "Name-501-2"},
    { "residentId": "id-502-1", "bedNo": "502-1",  "residentName": "Name-502-1"},
    { "residentId": "id-502-2", "bedNo": "502-2",  "residentName": "Name-502-2"},
    { "residentId": "id-502-3", "bedNo": "502-3",  "residentName": "Name-502-3"},
    { "residentId": "id-503-1", "bedNo": "503-1",  "residentName": "Name-503-1"},
    { "residentId": "id-503-2", "bedNo": "503-2",  "residentName": "Name-503-2"},
    { "residentId": "id-503-3", "bedNo": "503-3",  "residentName": "Name-503-3"},
    { "residentId": "id-504-1", "bedNo": "504-1",  "residentName": "Name-504-1"},
    { "residentId": "id-504-2", "bedNo": "504-2",  "residentName": "Name-504-2"},
    { "residentId": "id-504-3", "bedNo": "504-3",  "residentName": "Name-504-3"},
    { "residentId": "id-505-1", "bedNo": "505-1",  "residentName": "Name-505-1"},
    { "residentId": "id-505-2", "bedNo": "505-2",  "residentName": "Name-505-2"},
    { "residentId": "id-506-1", "bedNo": "506-1",  "residentName": "Name-506-1"},
    { "residentId": "id-506-2", "bedNo": "506-2",  "residentName": "Name-506-2"},
    { "residentId": "id-506-3", "bedNo": "506-3",  "residentName": "Name-506-3"},
    { "residentId": "id-507-1", "bedNo": "507-1",  "residentName": "Name-507-1"},
    { "residentId": "id-507-2", "bedNo": "507-2",  "residentName": "Name-507-2"},
    { "residentId": "id-507-3", "bedNo": "507-3",  "residentName": "Name-507-3"},
    { "residentId": "id-508-1", "bedNo": "508-1",  "residentName": "Name-508-1"},
    { "residentId": "id-508-2", "bedNo": "508-2",  "residentName": "Name-508-2"},
    { "residentId": "id-508-3", "bedNo": "508-3",  "residentName": "Name-508-3"},
    { "residentId": "id-508-4", "bedNo": "508-4",  "residentName": "Name-508-4"},
    { "residentId": "id-509-1", "bedNo": "509-1",  "residentName": "Name-509-1"},
    { "residentId": "id-509-2", "bedNo": "509-2",  "residentName": "Name-509-2"},
    { "residentId": "id-509-3", "bedNo": "509-3",  "residentName": "Name-509-3"},
    { "residentId": "id-510-1", "bedNo": "510-1",  "residentName": "Name-510-1"},
];

const bedSensorData = {
    "201-1": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "201-1",
        "oc1": [
            1
        ],
        "oc2": [
            0
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-01-19T10:25:59+00:00"
        ]
    },
    "202-2": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "202-2",
        "oc1": [
            0
        ],
        "oc2": [
            0
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-01-29T11:25:59+00:00"
        ]
    },
    "203-1": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "203-1",
        "oc1": [
            0
        ],
        "oc2": [
            0
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-01-20T10:25:59+00:00"
        ]
    },
    "302-2": {
        "heartRate": [
            0
        ],
        "heartRateVariance": [
            0
        ],
        "node": "302-2",
        "oc1": [
            0
        ],
        "oc2": [
            0
        ],
        "oc3": [
            0
        ],
        "respirationRate": [
            0
        ],
        "sigStrength": [
            24616
        ],
        "strokeVolume": [
            0
        ],
        "time": [
            "2018-01-20T10:25:59+00:00"
        ]
    }
};

//const bedSensorURL = 'http://astri.iohub.ml/data/pull';
const bedSensorURL = '/data/pull';
const patientsURL = '/endpoint/resident';
//const tLeftDict = {};

class BedSensorMonitorView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuItems: [
                { value: -1, label: '關' },
                { value: 0, label: '即時' },
                { value: 5, label: '5分鐘' },
                { value: 15, label: '15分鐘' },
                { value: 30, label: '30分鐘' },
            ],
            patientsInfo: [],
            timers: [],
            time: [],
            windowHeight: window.innerHeight,
        };
    }


    fetchPatientInfo = async () => {
        //let p = await axios.get(patientsURL);
        //let patients = p.data;
        if (!localStorage.getItem('alertSetting')) {
            let alertSettingInit = [];

            for (let i = 0; i < patients.length; i++) {
                let bedNo = patients[i].bedNo.toString();
                alertSettingInit.push({ id: bedNo, value: -1 });
            }
            localStorage.setItem('alertSetting', JSON.stringify(alertSettingInit));
        }

        try {
            let alertSetting = JSON.parse(localStorage.getItem('alertSetting'));
            let tLeftDict = JSON.parse(localStorage.getItem('alertTimer'));
            if (!tLeftDict) {
                tLeftDict = {};
            }

            //fetch data from server
            let r = await axios.get(bedSensorURL);
            let bedSensor = r.data;

            //let bedSensor = bedSensorData;
            let patientsInfo = [];

            for (let i = 0; i < patients.length; i++) {

                let p = patients[i];
                let bedNo = p.bedNo;
                if (bedNo) {
                    let bedNoArr = bedNo.split('-');
                    let lastN = bedNoArr[1];
                    let numStr = bedNoArr[0];
                    let bedSensorK = bedNo;// numStr + '-' + lastN;
                    if (bedSensor && bedSensor[bedSensorK]) {
                        let name = p.residentName;
                        // should check if state is in bed if yes then reset to 0
                        let time = new Date();
                        let leftPerior = '';
                        let isInBed = bedSensor[bedSensorK].oc2;
                        let leftT = new Date(bedSensor[bedSensorK].time);
                        //add to a global for recording left time of each patient
                        //check if we have record
                        if (tLeftDict && p.bedNo in tLeftDict) {
                            leftT = tLeftDict[p.bedNo]
                        } else { tLeftDict[p.bedNo] = time }
                        localStorage.setItem('alertTimer', JSON.stringify(tLeftDict));

                        let alertSelect = null;
                        for (var g = 0; g < alertSetting.length; g++) {
                            let alert = alertSetting[g];
                            if (alert.id === bedNo.toString()) {
                                alertSelect = alert.value;
                                break;
                            }
                        }
                        if (!alertSelect && alertSelect!=0) {
                            alertSelect = -1;
                            alertSetting.push({ id: bedNo.toString(), value: -1 });
                            localStorage.setItem('alertSetting', null);
                            localStorage.setItem('alertSetting', JSON.stringify(alertSetting));
                        }

                        let isActivateAlert = false;

                        if (isInBed[0] && isInBed[0] === 1) {
                            isInBed = true;
                            console.log(isInBed);
                            //so we update the last known inbed timestamp
                            leftT = new Date(bedSensor[bedSensorK].time);
                            tLeftDict[p.bedNo] = leftT;
                            localStorage.setItem('alertTimer', null);
                            localStorage.setItem('alertTimer', JSON.stringify(tLeftDict));
                        } else {
                            isInBed = false;
                            console.log(isInBed)
                            leftT = new Date(leftT);
                            leftPerior = (time.getTime() - leftT.getTime());

                            var difference = new Date(leftPerior);
                            var diff_mm = difference.getMinutes();
                            var diff_ss = difference.getSeconds();

                            console.log('p.bedNo::' + p.bedNo + 'leftPerior::' + leftPerior);

                            // format leave perior for display
                            if (leftPerior > 60 * 1000 * 60) {
                                leftPerior = Moment(leftT).format('YYYY-MM-DD hh:mm:ss a');
                            } else {
                                leftPerior = Moment(difference).format('mm:ss');
                            }
                            // if need alert (row get red) 
                            if (alertSelect != -1 && diff_mm >= alertSelect ) {
                                isActivateAlert = true;
                            }
                        }

                        let patientsInfoItem = { id: bedNo.toString(), name: name, roomNum: numStr, bedNum: lastN, time: leftPerior, isInBed: isInBed, alertSelect: alertSelect, isActivateAlert: isActivateAlert };
                        patientsInfo.push(patientsInfoItem);
                    }

                }
            }
            await this.setState({ patientsInfo: patientsInfo });
            //alert('patientsInfo:::' + JSON.stringify(patientsInfo));

        } catch (e) {
            console.log(e);
        }
    }

    componentWillMount = async () => {
        await this.fetchPatientInfo();
        this.intervalFetchData = setInterval(
            async () => await this.fetchPatientInfo(),
            10 * 1000
        );
        
        //alert(JSON.stringify(localStorage.getItem("data")));
    }

    componentDidMount = async () => {
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
        window.onresize = () => { this.setState({ windowHeight: window.innerHeight }); };
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
        clearInterval(this.intervalFetchData);
    }

    tick() {
        this.fetchPatientInfo()
        let timer = this.state.timers;
        let displayTimer = [];
        for (let i = 0; i < timer.length; i++) {
            let t = timer[i];
            let d = new Date(timer[i].time);
            let n = new Date();
            displayTimer.push({ id: t.id, time: this.timeFormat(n.getTime() - d.getTime()) });
        }

    }

    logChange = async (val) => {

        let alertSetting = JSON.parse(localStorage.getItem('alertSetting'));

        //alert('logChange:::' + JSON.stringify(val));
        if (alertSetting) {
            let value = val.value;
            let id = val.id;

            for (let j = 0; j < alertSetting.length; j++) {
                let alert = alertSetting[j];
                if (alert.id === id) {
                    alertSetting[j] = { id: id, value: value };
                    break;
                }
            }

            localStorage.setItem('alertSetting', null);
            localStorage.setItem('alertSetting', JSON.stringify(alertSetting));
        }

        let ps = this.state.patientsInfo;

        for (let i = 0; i < ps.length; i++) {
            if (ps[i].id === val.id) {
                ps[i].alertSelect = val.value;
                alert('Select:::' + JSON.stringify(val) + '***ps:::' + JSON.stringify(ps[i]));
                break;
            }
        }
        this.setState({ patientsInfo: ps });

    }

    _onClick = (id) => {
        let tLeftDict = JSON.parse(localStorage.getItem('alertTimer'));

        alert(JSON.stringify(tLeftDict));

        let patientsInfo = this.state.patientsInfo;
        for (let i = 0; i < patientsInfo.length; i++) {
            let p = patientsInfo[i];
            if (p.id === id) {
                p.isActivateAlert = false;
                tLeftDict[p.id] = new Date();
                localStorage.setItem('alertTimer', null);
                alert('new tLeftDict::' + JSON.stringify(tLeftDict));
                localStorage.setItem('alertTimer', JSON.stringify(tLeftDict));
                break;
            }
        }
        this.setState({ patientsInfo });
    }

    renderRow = (arr) => {
        return arr.map((patient, index) => {
            //select menu for alert time setting
            let menuItems = this.state.menuItems.map(item => item);
            for (let i = 0; i < menuItems.length; i++) {
                let op = menuItems[i];
                op = { ...menuItems[i], id: patient.id };
                menuItems[i] = op;
            }


            return (
                <tr key={patient.id} style={patient.isActivateAlert ? styles.tableRowWithAlert : styles.tableRow}>
                    <td style={patient.isActivateAlert ? styles.tableCellWithAlert : styles.tableCell}>{patient.roomNum}</td>
                    <td style={patient.isActivateAlert ? styles.tableCellWithAlert : styles.tableCell}>{patient.bedNum}</td>
                    <td style={patient.isActivateAlert ? styles.tableCellWithAlert : styles.tableCell}>{patient.name}</td>
                    <td style={styles.tableCell}>
                        <Select
                            value={patient.alertSelect}
                            options={menuItems}
                            onChange={this.logChange}
                        />
                    </td>
                    <td style={patient.isActivateAlert ? styles.tableCellWithAlert : styles.tableCell}>
                        {patient.isInBed && <div style={styles.pointIn}></div>}
                    </td>
                    <td style={styles.tableCell}>
                        {!patient.isInBed && <div style={styles.pointOff}></div>}
                    </td>
                    <td style={patient.isActivateAlert ? styles.tableCellWithAlert : styles.tableCell}>{patient.time}</td>
                    <td style={styles.tableCell}>
                        <Button style={styles.resetButton} onClick={() => this._onClick(patient.id)}>重置</Button>
                    </td>
                </tr>
            );
        });

    }

    renderPatientsTable = () => {
        return (
            <table className="table table-bordered" responsive='true' style={{ margin: 'auto', backgroundColor: '#FFFBF2', borderRadius: 5, }}>
                <thead style={{ backgroundColor: '#856147' }}>
                    <tr>
                        <td style={styles.tableColTitle}>房號</td>
                        <td style={styles.tableColTitle}>床號</td>
                        <td style={styles.tableColTitle}>院友姓名</td>
                        <td style={styles.tableColTitle}>提示設定</td>
                        <td style={styles.tableColTitle}>在床</td>
                        <td style={styles.tableColTitle}>離床</td>
                        <td style={styles.tableColTitle}>離床時間<span style={{ fontSize: 15 }}>(分鐘)</span></td>
                        <td style={styles.tableColTitle}>重置警報</td>
                    </tr>
                </thead>
                <tbody>
                    {this.renderRow(this.state.patientsInfo)}
                </tbody>
            </table>

        );
    }

    render() {
        return (
            <div style={{ flex: 1, backgroundColor: '#D8CBBB', minHeight: this.state.windowHeight, }}>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
                <link rel="stylesheet" href="https://unpkg.com/react-select/dist/react-select.css" />
                <div style={{ padding: 20, fontSize: 30, backgroundColor: '#60AB46', color: '#FFFFFF', textAlign: 'center' }}>
                    床墊感應儀
                </div>
                <div style={{ paddingBottom: 30, paddingTop: 80, paddingLeft: 80, paddingRight: 80, backgroundColor: '#FFFBF2', marginLeft: 30, marginRight: 30, minHeight: this.state.windowHeight - 110, }}>
                    {this.renderPatientsTable()}
                </div>

            </div >
        );
    }
}


export default BedSensorMonitorView;
