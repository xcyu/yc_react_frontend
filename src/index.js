import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import BedSensorMonitorView from './container/BedSensorMonitorView';

ReactDOM.render(<BedSensorMonitorView />, document.getElementById('root'));
registerServiceWorker();
